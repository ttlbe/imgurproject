import json
from asyncio.windows_events import NULL
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from behave import given, when, then
from selenium.webdriver.common.by import By

def use_creds(context):
    context.browser.get("https://api.imgur.com/oauth2/authorize?client_id=" + context.data["imgur_app"]["ID"] + "&response_type=token")

    username = context.data["imgur_app"]["email"]
    password = context.data["imgur_app"]["password"]

    try:
        cookies_agree = context.browser.find_element(By.CSS_SELECTOR,"button.css-47sehv")
        cookies_agree.click()
    except:
        print("cookies already closed")

    username_field = context.browser.find_element(By.CSS_SELECTOR,"input#username")
    username_field.clear()
    username_field.send_keys(username)

    password_field = context.browser.find_element(By.CSS_SELECTOR,"input#password")
    password_field.clear()
    password_field.send_keys(password)


    allow_button = context.browser.find_element(By.CSS_SELECTOR,"button#allow")
    allow_button.click()
    print()
    #totalstring = context.browser.current_url
    #token_list = totalstring.split("&")
    #context.list_clean = list(x.replace('https://imgur.com/#access_token=','access_token=') for x in token_list)
    #context.access_token = context.list_clean[0].replace('access_token=','')
    
#set baseURL
def before_all(context):
    context.data = {}
    with open("mappings.json") as dataFile:
        context.data = json.loads(dataFile.read())
    
    options = webdriver.ChromeOptions()
    options.add_argument("start-maximized")
    options.add_experimental_option('excludeSwitches', ['enable-logging']) 
    # options above are used to
    # -Start the browers maximized rather than a small window
    # -Surpress following ERROR messages
    # ERROR:ssl_client_socket_impl.cc(995) (handshake failed; returned -1, SSL error code 1, net_error -113)
    # ERROR:device_event_log_impl.cc(214)  (USB: usb_device_handle_win.cc:1049 Failed to read descriptor from node connection)
    # ERROR:device_event_log_impl.cc(214)  (Bluetooth: bluetooth_adapter_winrt.cc:1165 RequestRadioAccessAsync failed: RadioAccessStatus::DeniedByUserWill not be able to change radio power.)
    # 
    # Startup might feel slower because of these options
    # # 
    context.browser = webdriver.Chrome(ChromeDriverManager().install(),options=options)

    # ACCESS TOKEN GENERATION
    #use_creds(context)

def after_all(context):
    True
    context.browser.quit()

