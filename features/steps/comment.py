from gettext import find
from lib2to3.pgen2 import driver
import random
import time
from urllib import request, response
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from behave import given, when, then
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import requests

# /// SELENIUM TEST
@when("I comment on image via Selenium")
def comment_img_Sel(context):
    #context.browser = webdriver.Chrome(ChromeDriverManager().install())
    print()
    context.browser.get("https://imgur.com/gallery/T4Effai")
    time.sleep(3)
    # context.browser.refresh() # REFRESH WAS NIET GENOEG OM EEN ELEMENT TE VERWIJDEREN DAT ONS NIET LIET POSTEN
    context.browser.get("https://imgur.com/gallery/T4Effai") # DEZE MANIER WERKT WEL

    print()
    validate_ttl_img = context.browser.find_element(By.CSS_SELECTOR, "img[src='https://i.imgur.com/DnALIA6_d.webp?maxwidth=760&fidelity=grand']")
    # element = WebDriverWait(driver, 10).until(
    #     EC.visibility_of_element_located((validate_ttl_img))
    # )

    time.sleep(3)

    context.comments = list(context.browser.find_elements(By.CSS_SELECTOR, ".GalleryComment-body"))
    context.totalComments = len(context.comments)
    print()

    textarea = context.browser.find_element(By.CSS_SELECTOR,".Comment-create-textarea")
    print()
    context.random=random.randint(1,100)
    textarea.send_keys("nice test m8 i r8 8/8 i appreci8"+str(context.random))
    sendcomment = context.browser.find_element(By.CSS_SELECTOR,"button[title='post']")
    elem = context.browser.switch_to.active_element
    print()
    sendcomment.click()

    context.browser.refresh()

@then("I should check if a comment has been added via Selenium")
def comment_count_check_Sel(context):
    print()

    totalComments = len(list(context.browser.find_elements(By.CSS_SELECTOR, ".GalleryComment-body")))
    time.sleep(3)
    assert totalComments == (context.totalComments + 1), "comment count does not match ("+ str(totalComments) + " - " +str(context.totalComments + 1) + ")"


# /// API TEST
@when("I comment on image via API")
def comment_img(context):
    url = "https://api.imgur.com/3/comment"

    payload={'image_id': 'T4Effai', 'comment': 'API Test Comment'}
    files={}
    headers = {
    'Authorization': 'Bearer ' + context.data["imgur_app"]["access"]
    }
    context.info = requests.request("POST", url, headers=headers, data=payload, files=files)

    print()


# /// API TEST (API COUNT)
@when("I comment on image via API and count previous comments via API count method")
def comment_img_API_count(context):

    url = "https://api.imgur.com/3/comment"

    context.commentCount = get_account_comment_count_by_API(context)

    payload={'image_id': 'T4Effai', 'comment': 'API Test Comment (API Counting testing)'}
    files={}
    headers = {
    'Authorization': 'Bearer ' + context.data["imgur_app"]["access"]
    }
    context.info = requests.request("POST", url, headers=headers, data=payload, files=files)

    print()

@then("I should check if a comment has been added via API count method")
def comment_count_check_API_count(context):

    commentCount = get_account_comment_count_by_API(context)

    assert commentCount == (context.commentCount + 1), "Counts do not match due to API Error ("+ str(commentCount) + " - " +str(context.commentCount + 1) + ")"
    print()


# /// API TEST (MANUAL COUNT)
@when("I comment on image via API and count previous comments manually")
def comment_img_API_manual(context):

    url = "https://api.imgur.com/3/comment"

    context.commentCount = get_account_comment_count_by_ids(context)

    payload={'image_id': 'T4Effai', 'comment': 'API Test Comment (Manual Counting Testing)'}
    files={}
    headers = {
    'Authorization': 'Bearer ' + context.data["imgur_app"]["access"]
    }
    context.info = requests.request("POST", url, headers=headers, data=payload, files=files)

    print()

@then("I should check if a comment has been added via Manual count method")
def comment_count_check_API_manual(context):

    commentCount = get_account_comment_count_by_ids(context)

    assert commentCount == (context.commentCount + 1), "Counts do not match ("+ str(commentCount) + " - " +str(context.commentCount + 1) + ")"
    print()


# /// API TEST (SELENIUM COUNT)
@when("I comment on image via API and count previous comments via Selenium count method")
def comment_img_API_manual(context):

    url = "https://api.imgur.com/3/comment"

    context.commentCount = get_account_comment_count_by_selenium(context)

    payload={'image_id': 'T4Effai', 'comment': 'API Test Comment (Manual Counting Testing)'}
    files={}
    headers = {
    'Authorization': 'Bearer ' + context.data["imgur_app"]["access"]
    }
    context.info = requests.request("POST", url, headers=headers, data=payload, files=files)

    print()

@then("I should check if a comment has been added via Selenium count method")
def comment_count_check_API_manual(context):

    commentCount = get_account_comment_count_by_selenium(context)

    assert commentCount == (context.commentCount + 1), "Counts do not match ("+ str(commentCount) + " - " +str(context.commentCount + 1) + ")"
    print()


# /// USED FUNCTIONS
def get_account_comment_count_by_API(context):

    url = "https://api.imgur.com/3/account/" + context.data['imgur_app']['username'] + "/comments/count"

    payload={}
    files={}
    headers = { 'Authorization': 'Client-ID ' + context.data["imgur_app"]["ID"]}

    response = requests.request("GET", url, headers=headers, data=payload, files=files)
    return response.json()['data']
    print()

def get_account_comment_count_by_ids(context):

    url = "https://api.imgur.com/3/account/" + context.data['imgur_app']['username'] + "/comments"

    payload={}
    files={}
    headers = { 'Authorization': 'Client-ID ' + context.data["imgur_app"]["ID"]}

    response = requests.request("GET", url, headers=headers, data=payload, files=files)
    return len(response.json()['data'])
    print()

def get_account_comment_count_by_selenium(context):

    context.browser.get("https://imgur.com/gallery/T4Effai")
    time.sleep(3)
    return len(list(context.browser.find_elements(By.CSS_SELECTOR, ".GalleryComment-body")))

