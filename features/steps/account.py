from gettext import find
from lib2to3.pgen2 import driver
from behave import given, when, then
from selenium.webdriver.common.by import By
import requests

# // CHECK ACCOUNT INFORMATION
@given("I am logged in")
def get_acc(context):
    
    context.browser.get("https://imgur.com")
    try:
        cookies_agree = context.browser.find_element(By.CSS_SELECTOR,"button.css-47sehv")
        cookies_agree.click()
    except:
        print("cookies already closed")

    try:
        signin=context.browser.find_element(By.CSS_SELECTOR,".Navbar-signin")
        signin.click()
    except:
        signin=True
        print("already signed in")

    if signin != True:    

        username = context.data["imgur_app"]["email"]
        password = context.data["imgur_app"]["password"]



        username_field = context.browser.find_element(By.CSS_SELECTOR,"#username")
        username_field.clear()
        username_field.send_keys(username)

        password_field = context.browser.find_element(By.CSS_SELECTOR,"#password")
        password_field.clear()
        password_field.send_keys(password)


        allow_button = context.browser.find_element(By.CSS_SELECTOR,"#Imgur")
        allow_button.click()
        print()

@when("i ask for account information")
def ask_account_info(context):
    url = "https://api.imgur.com/3/account/" + context.data['imgur_app']['username']

    payload={}
    files={}
    headers = {
    'Authorization': 'Client-ID ' + context.data["imgur_app"]["ID"]
    }
    context.info = requests.request("GET", url, headers=headers, data=payload, files=files)
    print(context.info.text)
    print()

@then("I should get the correct status code")
def check__response_code(context):
    assert context.info.status_code == 200
    print()


#// CHECK BLOCK STATUS USER
@when("i check if my username is blocked")
def check_username_status(context):
    url = "https://api.imgur.com/account/v1/" + context.data['imgur_app']['username'] + "/block"

    payload={}
    files={}
    headers = {
    'Authorization': 'Bearer ' + context.data["imgur_app"]["access"]
    }
    context.info = requests.request("GET", url, headers=headers, data=payload, files=files)
    print(context.info.text)
    print()