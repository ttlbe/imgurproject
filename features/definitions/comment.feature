Feature: Comment

Scenario: Comment with Selenium
Given I am logged in
When I comment on image via Selenium
Then I should check if a comment has been added via Selenium

Scenario: Comment with API
When I comment on image via API
Then I should get the correct status code

Scenario: Comment with API with API counting method
When I comment on image via API and count previous comments via API count method
Then I should check if a comment has been added via API count method

Scenario: Comment with API with manual counting method
When I comment on image via API and count previous comments manually
Then I should check if a comment has been added via Manual count method

Scenario: Comment with API with Selenium counting method
When I comment on image via API and count previous comments via Selenium count method
Then I should check if a comment has been added via Selenium count method
