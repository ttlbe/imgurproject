Feature: Account

Scenario: Check block status
    Given I am logged in
    When i check if my username is blocked
    Then I should get the correct status code

Scenario: Check account information
    Given I am logged in
    When i ask for account information
    Then I should get the correct status code