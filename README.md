# README #

### What is this repository for? ###

* This project includes all files required to run the Imgur tests, both Selenium and API tests.
* Version 0.1

### How do I get set up? ###

* This project requires a python virtual environment with all dependencies installed to run
* After installing .venv don't forget to run "pip install -r requirements.txt" to install all dependencies
* See requirements.txt for requirements
* All tests are run via the debugger

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jumpstarters: Yannick, Ozan, Thomas
* Support: Rob, Jakub, Ann-Sophie